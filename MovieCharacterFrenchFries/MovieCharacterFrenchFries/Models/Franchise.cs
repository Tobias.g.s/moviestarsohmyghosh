﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterFrenchFries.Models
{
    public class Franchise
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [MaxLength(50)]
        public string Name { get; set; } = null!;
        [Required]
        [StringLength(250)]
        [MaxLength(250)]
        public string Description { get; set; } = null!;
        // relational
        public virtual ICollection<Movie> Movies { get; set; } = new HashSet<Movie>();
        

    }
}
