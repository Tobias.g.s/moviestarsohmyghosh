﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterFrenchFries.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [MaxLength(50)]
        public string Title { get; set; } = null!;
        [Required]
        [StringLength(50)]
        [MaxLength(50)]
        public string Genre { get; set; } = null!;
        public DateTime Release { get; set; }
        [Required]
        [StringLength(25)]
        [MaxLength(25)]
        public string Director { get; set; } = null!;
        [Required]
        [Url]
        [StringLength(2000)]
        [MaxLength(2000)]
        public string PosterUrl { get; set; } = null!;
        [Required]
        [Url]
        [StringLength(2000)]
        [MaxLength(2000)]
        public string TrailerUrl { get; set; } = null!;
        public int? FranchiseId { get; set; }

        // relational
        public virtual Franchise? Franchise { get; set; }
        public virtual ICollection<Character> Characters { get; set; } = new HashSet<Character>();

        
    }
}
