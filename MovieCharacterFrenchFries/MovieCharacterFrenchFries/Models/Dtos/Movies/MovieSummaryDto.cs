﻿namespace MovieCharacterFrenchFries.Models.Dtos.Movies
{
    public class MovieSummaryDto
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
    }
}
