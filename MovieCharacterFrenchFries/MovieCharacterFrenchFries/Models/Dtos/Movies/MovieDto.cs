﻿using MovieCharacterFrenchFries.Models.Dtos.Characters;
using MovieCharacterFrenchFries.Models.Dtos.Franchises;

namespace MovieCharacterFrenchFries.Models.Dtos.Movies
{
    public class MovieDto
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string Genre { get; set; } = null!;
        public DateTime Release { get; set; }
        public string Director { get; set; } = null!;
        public string TrailerUrl { get; set; } = null!;
        public int FranchiseId { get; set; }
        public ICollection<CharacterSummaryDto> Characters { get; set; } = null!;

    }
}
