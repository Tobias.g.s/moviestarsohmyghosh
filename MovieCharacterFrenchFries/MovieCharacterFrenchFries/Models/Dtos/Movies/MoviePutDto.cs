﻿namespace MovieCharacterFrenchFries.Models.Dtos.Movies
{
    public class MoviePutDto
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string Genre { get; set; } = null!;
        public DateTime Release { get; set; }
        public string Director { get; set; } = null!;
        public string PosterUrl { get; set; } = null!;
        public string TrailerUrl { get; set; } = null!;
        public int FranchiseId { get; set; }
    }
}
