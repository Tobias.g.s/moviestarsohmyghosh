﻿namespace MovieCharacterFrenchFries.Models.Dtos.Franchises
{
    public class FranchiseSummaryDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
