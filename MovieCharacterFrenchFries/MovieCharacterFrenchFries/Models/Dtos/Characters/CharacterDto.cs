﻿using MovieCharacterFrenchFries.Models.Dtos.Movies;

namespace MovieCharacterFrenchFries.Models.Dtos.Characters
{
    public class CharacterDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Alias { get; set; }
        public string gender { get; set; } = null!;
        public ICollection<MovieSummaryDto> Movies { get; set; } = null!;
    }
}
