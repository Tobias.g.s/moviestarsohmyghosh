﻿namespace MovieCharacterFrenchFries.Models.Dtos.Characters
{
    public class CharacterPutDto
    {
        public int Id { get; set; }
        public string? Alias { get; set; }
        public string Gender { get; set; } = null!;
        public int[] Movies { get; set; } = null!;
    }
}
