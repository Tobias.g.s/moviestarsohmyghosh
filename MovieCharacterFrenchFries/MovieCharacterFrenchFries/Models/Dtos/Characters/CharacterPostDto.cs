﻿namespace MovieCharacterFrenchFries.Models.Dtos.Characters
{
    public class CharacterPostDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Alias { get; set; }
        public string Gender { get; set; } = null!;
    }
}
