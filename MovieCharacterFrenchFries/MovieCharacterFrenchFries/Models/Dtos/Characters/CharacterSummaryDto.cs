﻿namespace MovieCharacterFrenchFries.Models.Dtos.Characters
{
    public class CharacterSummaryDto
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
    }
}
