﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterFrenchFries.Models
{
    public class Character
    {  
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        [MaxLength(50)]
        public string Name { get; set; } = null!;
        [StringLength(50)]
        [MaxLength(50)]
        public string? Alias { get; set; }
        [Required]
        [StringLength(50)]
        [MaxLength(50)]
        public string gender = null!;
        // close to the max length of a url , spam prevention should be handled through buisness logic which checks for valid domain.
        [Url]
        [StringLength(2000)]
        [MaxLength(2000)]
        public string? ImageUrl { get; set; }
        // relational
        public virtual ICollection<Movie> Movies { get; set; } = new HashSet<Movie>();
    }

}
