﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MovieCharacterFrenchFries.Models;

namespace MovieCharacterFrenchFries.Data
{
    public class MovieCharacterFranchiseContext : DbContext
    {

        public MovieCharacterFranchiseContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>().HasData(
               new Character()
               {
                   Id = 1, Name = "Neo", gender ="Male"
               },
               new Character()
               {
                   Id = 2,
                   Name = "The Arcitect",
                   gender = "Male"
               },
               new Character()
               {
                   Id = 3,
                   Name = "Apoc",
                   gender = "Male"
               }
           );

            modelBuilder.Entity<Franchise>().HasData(
                new Franchise()
                {
                    Id = 1,
                    Name = "The Matrix",
                    Description = "Uhm Robots kinda took over and humans are like totally mid atm, but this Neo dude is toatlly redpilling the matrix which is kinda based."
                },
                new Franchise()
                {
                    Id = 2,
                    Name = "Sharknado",
                    Description = "Philosphical documantation of the human condition."
                }
            );
            modelBuilder.Entity<Movie>().HasData(
                new Movie()
                {
                    Id = 1,
                    Title = "The Matrix",
                    Genre = "Fantasy",
                    Release = new DateTime(1999, 3, 24),
                    Director = "Nichlas Bay",
                    // look at it nick.
                    PosterUrl = "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg",
                    TrailerUrl = "https://www.youtube.com/watch?v=xm3YgoEiEDc",
                    FranchiseId = 1
                },
                new Movie()
                {
                    Id = 2,
                    Title = "The Matrix Reloaded",
                    Genre = "Fantasy",
                    Release = new DateTime(2003, 3, 24),
                    Director = "Nichlas Bay",
                    // look at it nick.
                    PosterUrl = "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg",
                    TrailerUrl = "https://www.youtube.com/watch?v=xm3YgoEiEDc",
                    FranchiseId = 1
                },
                 new Movie()
                 {
                     Id = 3,
                     Title = "The Matrix Revolutions",
                     Genre = "Fantasy",
                     Release = new DateTime(2003, 3, 24),
                     Director = "Nichlas Bay",
                     // look at it nick.
                     PosterUrl = "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg",
                     TrailerUrl = "https://www.youtube.com/watch?v=xm3YgoEiEDc",
                     FranchiseId = 1
                 },
                 new Movie()
                 {
                     Id = 4,
                     Title = "Sharknado",
                     Genre = "Documentary",
                     Release = new DateTime(2003, 3, 24),
                     Director = "Dean Schineder",
                     // look at it nick.
                     PosterUrl = "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg",
                     TrailerUrl = "https://www.youtube.com/watch?v=xm3YgoEiEDc",
                     FranchiseId = 2
                 },
                 new Movie()
                 {
                     Id = 5,
                     Title = "Sharknado 2: the feeding frenzy",
                     Genre = "Documentary",
                     Release = new DateTime(2003, 3, 24),
                     Director = "Dean Schineder",
                     // look at it nick.
                     PosterUrl = "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg",
                     TrailerUrl = "https://www.youtube.com/watch?v=xm3YgoEiEDc",
                     FranchiseId = 2
                 }
            );

            modelBuilder.Entity<Character>()
               .HasMany(c => c.Movies)
               .WithMany(m => m.Characters)
               .UsingEntity<Dictionary<string, object>>(
                   "CharacterMovie",
                   l => l.HasOne<Movie>().WithMany().HasForeignKey("MoviesId"),
                   r => r.HasOne<Character>().WithMany().HasForeignKey("CharactersId"),
                   je =>
                   {
                       je.HasKey("CharactersId", "MoviesId");
                       je.HasData(
                           new {CharactersId = 1, MoviesId = 1},
                           new { CharactersId = 1, MoviesId = 2 },
                           new { CharactersId = 1, MoviesId = 3 },
                           new { CharactersId = 2, MoviesId = 3 },
                           new { CharactersId = 3, MoviesId = 1 }
                           );
                   }
               );


        }

        public virtual DbSet<Movie> Movies { get; set; }
        public virtual DbSet<Franchise> Franchises { get;set; }
        public virtual DbSet<Character> Characters { get; set; }
    }
}
