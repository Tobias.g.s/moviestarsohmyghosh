﻿namespace MovieCharacterFrenchFries.Services
{
    // its incrudeble 🤠
    public interface ICrudService<T, ID>
    {
        public Task<ICollection<T>> GetAllAsync();
        public Task<T>GetByIdAsync(ID id);
        public Task AddAsync(T entity);
        public Task DeleteByIdAsync(ID id);
        public Task UpdateAsync(T entity);
            
    }
}
