﻿using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Models.Dtos.Movies;

namespace MovieCharacterFrenchFries.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        public Task<ICollection<Character>> GetCharacters(int movieId);
        public Task<Franchise?> GetFranchise(int movieId);
        public Task UpdateCharactersAsync(int[] characterIds, int movieId);
    }
}
