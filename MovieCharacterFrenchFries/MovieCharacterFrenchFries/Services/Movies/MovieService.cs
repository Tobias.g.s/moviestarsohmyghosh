﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MovieCharacterFrenchFries.Data;
using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Models.Dtos.Movies;
using MovieCharacterFrenchFries.Utils.Exceptions;

namespace MovieCharacterFrenchFries.Services.Movies
{ 
    public class MovieService : IMovieService
    {
        private readonly MovieCharacterFranchiseContext _context;
        public MovieService(MovieCharacterFranchiseContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Movie movie)
        {
            await _context.AddAsync(movie);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            if (!await MovieExistsAsync(id))
            {
                throw new MovieNotFoundException();
            }
            var entity = await GetByIdAsync(id);
            _context.Remove(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Movie>> GetAllAsync() 
            => await _context.Movies.Include(m => m.Characters).ToListAsync();

        public async Task<Movie> GetByIdAsync(int id)
            => (await MovieExistsAsync(id))? await _context.Movies.Where(movie => movie.Id == id).Include(m => m.Characters).FirstAsync() : throw new MovieNotFoundException();
        

        public async Task<ICollection<Character>> GetCharacters(int movieId)
            => (await MovieExistsAsync(movieId))? await _context.Movies.Where(movie => movie.Id == movieId).Include(m => m.Characters).Select(movie => movie.Characters).FirstAsync() : throw new MovieNotFoundException();

        public async Task<Franchise?> GetFranchise(int movieId)
            => (await MovieExistsAsync(movieId)) ? await _context.Movies.Where(movie => movie.Id == movieId).Include(m => m.Franchise).Select(movie => movie.Franchise).FirstAsync() : throw new MovieNotFoundException();

        public async Task UpdateAsync(Movie entity)
        {
            if (!await MovieExistsAsync(entity.Id)) throw new MovieNotFoundException();
            
            _context.Update(entity);
            await _context.SaveChangesAsync();
        }

        private async Task<bool> MovieExistsAsync(int id)
        {
            return await _context.Movies.AnyAsync(e => e.Id == id);
        }

        public async Task UpdateCharactersAsync(int[] characterIds, int movieId)
        {
            if (!await MovieExistsAsync(movieId))
            {
                throw new MovieNotFoundException();
            }
            List<Character> characters = characterIds.ToList()
                .Select(cId => _context.Characters
                .Where(c => c.Id == cId).First())
                .ToList();

            Movie movie = await _context.Movies
                .Where(m => m.Id == movieId).Include(m => m.Characters)
                .FirstAsync();
            movie.Characters = characters;
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
