﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterFrenchFries.Data;
using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Utils.Exceptions;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.Versioning;

namespace MovieCharacterFrenchFries.Services.Franchises
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieCharacterFranchiseContext _context;

        public FranchiseService(MovieCharacterFranchiseContext context)
        {
            _context = context;
        }

        public async Task DeleteByIdAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null) { throw new FranchiseNotFoundException(); }
            _context.Remove<Franchise>(franchise);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Franchise>> GetAllAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            if (!await FranchiseExistsAsync(id))
            {
                throw new FranchiseNotFoundException();
            }
            return await _context.Franchises
                .Where(f => f.Id == id)
                .Include(f => f.Movies)
                .FirstAsync();
        }

        public async Task<ICollection<Movie>> GetMoviesAsync(int id)
        {
            if (!await FranchiseExistsAsync(id))
            {
               throw new FranchiseNotFoundException();
            }
            return await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();
        }

        public async Task<ICollection<Character>> GetUniqueCharactersAsync(int id)
        {

            if (!await FranchiseExistsAsync(id))
            {
                throw new FranchiseNotFoundException();
            }
            Franchise franchise = await _context.Franchises
                .Where(f => f.Id == id)
                .Include(f => f.Movies)
                .ThenInclude(m => m.Characters)
                .FirstAsync();
            
            ICollection<Character> characters = new List<Character>();
            foreach (var movie in franchise.Movies)
            {
                foreach (var character in movie.Characters)
                {
                    if (!characters.Contains(character))
                    {
                        characters.Add(character);
                    }
                }
            }
            return characters.ToList();
        }

        public async Task UpdateAsync(Franchise franchise)
        {
            if (!await FranchiseExistsAsync(franchise.Id))
            {
                throw new FranchiseNotFoundException();
            }
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task AddAsync(Franchise franchise)
        {
            await _context.AddAsync(franchise);
            await _context.SaveChangesAsync();
        }

        private async Task<bool> FranchiseExistsAsync(int id)
        {
            return await _context.Franchises.AnyAsync(e => e.Id == id);
        }

        public async Task UpdateMoviesAsync(int[] movieIds ,int franchiseId)
        {
            if (!await FranchiseExistsAsync(franchiseId))
            {
                throw new FranchiseNotFoundException();
            }
            List<Movie> movies = movieIds.ToList()
                .Select(mId => _context.Movies
                .Where(m => m.Id == mId).First())
                .ToList();
            Franchise franchise = await _context.Franchises
                .Where(f => f.Id == franchiseId)
                .FirstAsync();
            franchise.Movies = movies;
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
