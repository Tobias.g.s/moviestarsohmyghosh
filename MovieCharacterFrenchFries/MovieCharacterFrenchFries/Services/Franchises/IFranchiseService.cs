﻿using MovieCharacterFrenchFries.Models;

namespace MovieCharacterFrenchFries.Services.Franchises
{
    public interface IFranchiseService : ICrudService<Franchise, int>
    {
        public Task<ICollection<Movie>> GetMoviesAsync(int id);
        public Task<ICollection<Character>> GetUniqueCharactersAsync(int id);
        public Task UpdateMoviesAsync( int[] movieIds, int franchiseId);
        
    }
}
