﻿using MovieCharacterFrenchFries.Models;

namespace MovieCharacterFrenchFries.Services.Characters
{
    public interface ICharacterService : ICrudService<Character, int>
    {
        Task<ICollection<Movie>> GetMovies(int characterId);
    }
}
