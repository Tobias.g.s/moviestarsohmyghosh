﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterFrenchFries.Data;
using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Utils.Exceptions;

namespace MovieCharacterFrenchFries.Services.Characters
{
    public class CharacterService : ICharacterService
    {

        private readonly MovieCharacterFranchiseContext _context;

        public CharacterService(MovieCharacterFranchiseContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Character entity)
        {
            await _context.Characters.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            if (await CharacterExistsAsync(id)) 
            {
                throw new CharacterNotFoundException();
            }
            _context.Remove(id);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<Character>> GetAllAsync()
            => await _context.Characters.Include(c => c.Movies).ToListAsync();

        public async Task<Character> GetByIdAsync(int id)
            => (await CharacterExistsAsync(id))? await _context.Characters.Where(character => character.Id == id).Include(c => c.Movies).FirstAsync() : throw new CharacterNotFoundException();

        public async Task<ICollection<Movie>> GetMovies(int characterId)
            => (await CharacterExistsAsync(characterId)) ? await _context.Characters.Where(character => character.Id == characterId).Include(c => c.Movies).Select(character => character.Movies).FirstAsync(): throw new CharacterNotFoundException();

        public async Task UpdateAsync(Character character)
        {
            if (!await CharacterExistsAsync(character.Id))
            {
                throw new CharacterNotFoundException();
            }
            _context.Update(character);
            await _context.SaveChangesAsync();
        }
        private async Task<bool> CharacterExistsAsync(int id)
        {
            return await _context.Characters.AnyAsync(c => c.Id == id);
        }
    }
}
