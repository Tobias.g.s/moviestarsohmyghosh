﻿namespace MovieCharacterFrenchFries.Utils.Exceptions
{
    /// <summary>
    /// Thrown when No Character of given ID is found
    /// </summary>
    public class CharacterNotFoundException : EntityNotFoundException
    {
        public CharacterNotFoundException() : base("No character with that Id was not found."){ }
    }

}
