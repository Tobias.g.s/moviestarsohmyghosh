﻿namespace MovieCharacterFrenchFries.Utils.Exceptions
{
    /// <summary>
    /// Thrown when No Entity of given ID is found
    /// </summary>
    public class EntityNotFoundException : NullReferenceException
    {
        public EntityNotFoundException(string? message) : base(message) { }
    }
}
