﻿namespace MovieCharacterFrenchFries.Utils.Exceptions
{
    /// <summary>
    /// Thrown when No Franchise of given ID is found
    /// </summary>
    public class FranchiseNotFoundException : EntityNotFoundException
    {
        public FranchiseNotFoundException():base("No franchise with that Id was not found.") { }
    }
}
