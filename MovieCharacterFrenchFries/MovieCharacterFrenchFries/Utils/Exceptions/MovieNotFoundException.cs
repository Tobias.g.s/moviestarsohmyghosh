﻿namespace MovieCharacterFrenchFries.Utils.Exceptions
{
    /// <summary>
    /// Thrown when No Movie of given ID is found
    /// </summary>
    public class MovieNotFoundException : EntityNotFoundException
    {
        public MovieNotFoundException() : base("No movie with that Id was not found."){ }
    }
}
