﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace MovieCharacterFrenchFries.Migrations
{
    /// <inheritdoc />
    public partial class create : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "PosterUrl", "Release", "Title", "TrailerUrl" },
                values: new object[,]
                {
                    { 2, "Nichlas Bay", 1, "Fantasy", "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg", new DateTime(2003, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Matrix Reloaded", "https://www.youtube.com/watch?v=xm3YgoEiEDc" },
                    { 3, "Nichlas Bay", 1, "Fantasy", "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg", new DateTime(2003, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Matrix Revolutions", "https://www.youtube.com/watch?v=xm3YgoEiEDc" },
                    { 4, "Dean Schineder", 2, "Documentary", "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg", new DateTime(2003, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sharknado", "https://www.youtube.com/watch?v=xm3YgoEiEDc" },
                    { 5, "Dean Schineder", 2, "Documentary", "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg", new DateTime(2003, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sharknado 2: the feeding frenzy", "https://www.youtube.com/watch?v=xm3YgoEiEDc" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
