﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace MovieCharacterFrenchFries.Migrations
{
    /// <inheritdoc />
    public partial class seedingData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Uhm Robots kinda took over and humans are like totally mid atm, but this Neo dude is toatlly redpilling the matrix which is kinda based.", "The Matrix" },
                    { 2, "Philosphical documantation of the human condition.", "Sharknado" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "PosterUrl", "Release", "Title", "TrailerUrl" },
                values: new object[] { 1, "Nichlas Bay", 1, "Fantasy", "https://wompampsupport.azureedge.net/fetchimage?siteId=7575&v=2&jpgQuality=100&width=700&url=https%3A%2F%2Fi.kym-cdn.com%2Fentries%2Ficons%2Ffacebook%2F000%2F014%2F409%2Fphteven.jpg", new DateTime(1999, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Matrix", "https://www.youtube.com/watch?v=xm3YgoEiEDc" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
