﻿using AutoMapper;
using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Models.Dtos.Franchises;

namespace MovieCharacterFrenchFries.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchisePostDto, Franchise>();

            CreateMap<Franchise, FranchiseDto>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.Id).ToArray()));

            CreateMap<FranchisePutDto, Franchise>();
        }
    }
}
