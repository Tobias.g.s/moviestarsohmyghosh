﻿
using AutoMapper;
using MovieCharacterFrenchFries.Data;
using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Models.Dtos.Franchises;
using MovieCharacterFrenchFries.Models.Dtos.Movies;

namespace MovieCharacterFrenchFries.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile() {
            CreateMap<Movie, MovieDto>()
                .ForMember(dto => dto.Characters, opt => opt
                .MapFrom(m => m.Characters)
                );
            CreateMap<Movie, MovieSummaryDto>();
            CreateMap<MoviePostDto, Movie>();
            CreateMap<MoviePutDto, Movie>();
        }
    }
}
