﻿using AutoMapper;
using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Models.Dtos.Characters;

namespace MovieCharacterFrenchFries.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile() {
            CreateMap<Character, CharacterDto>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(c => c.Movies));
            CreateMap<Character, CharacterSummaryDto>();
            CreateMap<CharacterPostDto, Character>();
            CreateMap<CharacterPutDto, Character>();
        }
    }
}
