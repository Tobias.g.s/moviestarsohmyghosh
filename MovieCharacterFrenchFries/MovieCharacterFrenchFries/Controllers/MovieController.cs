﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Models.Dtos.Characters;
using MovieCharacterFrenchFries.Models.Dtos.Franchises;
using MovieCharacterFrenchFries.Models.Dtos.Movies;
using MovieCharacterFrenchFries.Services.Movies;
using System.Net;
using System.Net.Mime;

namespace MovieCharacterFrenchFries.Controllers
{
    [Route("movies")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MovieController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MovieController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }


        /// <summary>
        /// Get all movies in the database
        /// </summary>
        /// <returns> List of movies </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetMovies()
            => Ok(_mapper.Map<List<MovieDto>>(await _movieService.GetAllAsync()));

        /// <summary>
        /// Gets a specific movie by its id
        /// </summary>
        /// <param name="id"> </param>
        /// <returns> A movie dto </returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetMovie(int id)
            => Ok(_mapper.Map<MovieDto>(await _movieService.GetByIdAsync(id)));

        /// <summary>
        /// Posts a movie to the database
        /// </summary>
        /// <param name="movie"> Movie post dto, the required data to add a movie </param>
        /// <returns> Movie added </returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPost]
        public async Task<ActionResult> AddMovie(MoviePostDto movie)
        {
            Movie m = _mapper.Map<Movie>(movie);
            await _movieService.AddAsync(m);
            return CreatedAtAction("GetMovie", new { id = m.Id}, _mapper.Map<MovieDto>(m));
        }

        /// <summary>
        /// Permanently deletes a movie from the database (NOT soft-delete)
        /// </summary>
        /// <param name="id"> id of the movie to delete</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMovie(int id)
        {
            await _movieService.DeleteByIdAsync(id);
            return Ok();
        }

        /// <summary>
        /// Gets all characters for a specific movie id
        /// </summary>
        /// <param name="movieId">The id of the movie</param>
        /// <returns>HashSet of characters playing in the specified movie</returns>
         [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{movieId}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterSummaryDto>>> GetCharacters(int movieId)
             => Ok(_mapper.Map<IEnumerable<CharacterSummaryDto>>(await _movieService.GetCharacters(movieId)));

        //[HttpGet("{movieId}/franchise")]
        //public async Task<ActionResult<FranchiseDto?>> GetFranchise(int movieId)
        //    => Ok(_mapper.Map<FranchiseSummaryDto>(await _movieService.GetFranchise(movieId)));

        /// <summary>
        /// Updates/posts movie data at the specified id
        /// </summary>
        /// <param name="entity"> Id, and the data to update </param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status415UnsupportedMediaType)]
        [HttpPut]
        public async Task<ActionResult> UpdateAsync(MoviePutDto entity)
        {
            await _movieService.UpdateAsync(_mapper.Map<Movie>(entity));
            return Ok();
        }
        /// <summary>
        /// Update the list of characters in a spesific movie.
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status415UnsupportedMediaType)]
        [HttpPut("{id}/characters")]
        public async Task<ActionResult> UpdateCharactersInMovieAsync(int[] characterIds, int id)
        {
            try
            {
                await _movieService.UpdateCharactersAsync(characterIds, id);
                return NoContent();
            }
             catch (Exception ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
    }
}
