﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterFrenchFries.Data;
using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Models.Dtos.Characters;
using MovieCharacterFrenchFries.Models.Dtos.Franchises;
using MovieCharacterFrenchFries.Models.Dtos.Movies;
using MovieCharacterFrenchFries.Services.Franchises;

namespace MovieCharacterFrenchFries.Controllers
{
    [Route("api/franchise")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchiseController : ControllerBase
    {
        private readonly FranchiseService _service;
        private readonly IMapper _mapper;

        public FranchiseController(MovieCharacterFranchiseContext context, IFranchiseService service, IMapper mapper)
        {
            _service = (FranchiseService)service;
            _mapper = mapper;
        }
        /// <summary>
        /// Gets all franchises in Database.
        /// </summary>
        /// <returns></returns>
        // GET: api/Franchise
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDto>>> GetFranchises()
        {
            return Ok(
                    _mapper.Map<List<FranchiseDto>>(await _service.GetAllAsync())
                );
        }
        /// <summary>
        /// Gets spesific franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Franchise/5
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDto>> GetFranchiseByIdAsync(int id)
        {
            return Ok(_mapper.Map<FranchiseDto>(await _service.GetByIdAsync(id)));
        }
        /// <summary>
        /// Updates existing franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        // PUT: api/Franchise/5
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchiseAsync(int id, FranchisePutDto franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }
            try
            {
                await _service.UpdateAsync(
                        _mapper.Map<Franchise>(franchise)
                    );
                return NoContent();
            }
            catch (Exception ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }
        /// <summary>
        /// Adds Franchise to the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        // POST: api/Franchise
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status415UnsupportedMediaType)]
        [HttpPost]
        public async Task<ActionResult<FranchiseDto>> PostFranchise(FranchisePostDto entity)
        {
            // Mapping done separately to use the object in created at action
            Franchise franchise = _mapper.Map<Franchise>(entity);
            await _service.AddAsync(franchise);
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, _mapper.Map<FranchiseDto>(franchise));
        }
        /// <summary>
        /// Deletes Fanchise by given Id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns></returns>
        // DELETE: api/Franchise/5
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            await _service.DeleteByIdAsync(id);
            return NoContent();
        }
        /// <summary>
        /// Gets all movies in given franchise.
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <returns>List of movies in franchise.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<MovieSummaryDto>> GetMoviesInFranchise(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<MovieSummaryDto>>(await _service.GetMoviesAsync(id))
                    );
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Gets all characters in all movies in given franchise.
        /// </summary>
        /// <param name="id">Franchise id to get characters from.</param>
        /// <returns>List of character ids and names</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<CharacterSummaryDto>> GetCharactersInFranchise(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<CharacterSummaryDto>>(await _service.GetUniqueCharactersAsync(id))
                    );
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Updates the list of movies for franchise with given id.
        /// </summary>
        /// <param name="MovietIds">Int array containing the movies to add to franchise.</param>
        /// <param name="id"> Franchise to update.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesForFranchiseAsync(int[] MovietIds, int id)
        {
            try
            {
                await _service.UpdateMoviesAsync(MovietIds, id);
                return NoContent();
            }
            catch (Exception ex)
            {
                // Formatting an error code for the exception messages.
                // Using the built in Problem Details.
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = ((int)HttpStatusCode.NotFound)
                    }
                    );
            }
        }


    }
}
