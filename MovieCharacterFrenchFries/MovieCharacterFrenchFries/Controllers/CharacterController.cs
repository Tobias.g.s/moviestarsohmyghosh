﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharacterFrenchFries.Models;
using MovieCharacterFrenchFries.Models.Dtos.Characters;
using MovieCharacterFrenchFries.Services.Characters;
using System.Net.Mime;

namespace MovieCharacterFrenchFries.Controllers
{
    [Route("characters")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharacterController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharacterController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Gets all Characters from DB.
        /// </summary>
        /// <returns>List of CharacterDtos</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]   
        [HttpGet]
        public async Task<ActionResult<ICollection<CharacterDto>>> GetCharactersAsync()
            => Ok(_mapper.Map<List<CharacterDto>>(await _characterService.GetAllAsync()));
        /// <summary>
        /// Gets spesific character by Id
        /// </summary>
        /// <param name="id">ID</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDto>> GetCharacterByIdAsync(int id)
            => Ok(_mapper.Map<CharacterDto>(await _characterService.GetByIdAsync(id)));
        /// <summary>
        /// Adds Character to DB
        /// </summary>
        /// <param name="character">CharacterDTO</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status415UnsupportedMediaType)]
        [HttpPost]
        public async Task<ActionResult> AddCharacterAsync(CharacterPostDto character)
        {
            Character c = _mapper.Map<Character>(character);
            await _characterService.AddAsync(c);
            return CreatedAtAction("GetCharacter", new { id = c.Id }, _mapper.Map<CharacterDto>(c));
        }
        /// <summary>
        /// Updates an existing character in db.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut]
        public async Task<ActionResult> UpdateCharacterAsync(int id ,CharacterPutDto character)
        {
            if (id != character.Id)
            {
                return BadRequest();    
            }
            await _characterService.UpdateAsync(_mapper.Map<Character>(character));
            return Ok();
        }
        /// <summary>
        /// Deletes an existing character in db.
        /// </summary>
        /// <param name="characterId"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete]
        public async Task<ActionResult> DeleteCharacterAsync(int characterId)
        {
            await _characterService.DeleteByIdAsync(characterId);
            return Ok();
        }
    }
}
