using AutoMapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using MovieCharacterFrenchFries.Data;
using MovieCharacterFrenchFries.Profiles;
using MovieCharacterFrenchFries.Services.Characters;
using MovieCharacterFrenchFries.Services.Franchises;
using MovieCharacterFrenchFries.Services.Movies;
using System.Reflection;

namespace MovieCharacterFrenchFries
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

            // Add services to the container.

            builder.Services.AddControllers();

            #region DATABASE_CONFIGURATION
            // DpInjection of dbContext
            // just hook it into my veins
            builder.Services.AddDbContext<MovieCharacterFranchiseContext>(
                options =>  options.UseSqlServer(builder.Configuration.GetConnectionString("Default")));
            #endregion

            #region SERVICE_CONFIGURATION
            builder.Services.AddScoped<IMovieService, MovieService>();
            builder.Services.AddScoped<ICharacterService, CharacterService>();
            builder.Services.AddTransient<IFranchiseService, FranchiseService>();
            #endregion

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Franchises and movies pluss characters API",
                    Description = "Simple API to manage Movies , characters and franchises studies",
                    Contact = new OpenApiContact
                    {
                        Name = "Tobias and bror",
                        Url = new Uri("https://google.com")
                    },
                    License = new OpenApiLicense
                    {
                        Name = "None Kekw",
                        Url = new Uri("https://opensource.org/licenses/MIT")
                    }
                });
                options.IncludeXmlComments(xmlPath);
            }
            );
            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();
            app.Run();            
            
        }
    }
}